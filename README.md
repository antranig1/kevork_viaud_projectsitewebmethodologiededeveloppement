# Movie_App

Prérequis pour utilisé l'application : npm et git


Ouvrez un terminal.

## Clonez le projet dans un dossier
```
git clone ...
```

## Initialisation des packages
```
npm i
```

## Lancement du serveur : 
```
npm run serve
```

#### Lancer un navigateur à l'adresse indiquée par npm : 
```
http://localhost:port
```
## Bonne recherche !